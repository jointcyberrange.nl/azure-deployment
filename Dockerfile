# Use a base image with Azure CLI pre-installed
FROM mcr.microsoft.com/azure-cli
# RUN curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl
# RUN chmod +x ./kubectl
# RUN mv ./kubectl /usr/local/bin

# Set the working directory
WORKDIR /workspace

# Add any custom configurations or scripts as needed

# Set the entrypoint command
CMD [ "bash" ]