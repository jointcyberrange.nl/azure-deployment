
Instructions for setting up your AKS for JCR container challenges. 

Subset of https://learn.microsoft.com/en-us/azure/aks/use-node-public-ips

# Installation

```az aks list -o table```

Not sure if all of these are needed, and how often. Looks like once per subscription.
No, it looks more like once per installation of the az cli.
All of them seem to matter.
```bash az
az extension add --name aks-preview
az extension update --name aks-preview

az feature register --namespace "Microsoft.ContainerService" --name "NodePublicIPTagsPreview"
az feature show --namespace "Microsoft.ContainerService" --name "NodePublicIPTagsPreview"
az provider register --namespace Microsoft.ContainerService

az feature register --namespace "Microsoft.ContainerService" --name "NodePublicIPNSGControlPreview"
az feature show --namespace "Microsoft.ContainerService" --name "NodePublicIPNSGControlPreview"
az provider register --namespace Microsoft.ContainerService

az feature register --namespace "Microsoft.ContainerService" --name "PodHostPortAutoAssignPreview"
az feature show --namespace "Microsoft.ContainerService" --name "PodHostPortAutoAssignPreview"
az provider register --namespace Microsoft.ContainerService

```

Current setup
```bash
az aks create --resource-group JCR-staging --name jcr-argotest22-peter \
--enable-node-public-ip --generate-ssh-keys --nodepool-name argo22 --node-count 2 \
--node-vm-size standard_E2as_v4 \
--nodepool-allowed-host-ports 80/tcp,443/tcp,53/udp,30000-60000/tcp,30000-50000/udp \
-l westeurope --enable-addons http_application_routing 

export KUBECONFIG=/root/.kube/config
export KUBECONFIG=/usr/local/share/kube-localhost/config
az aks get-credentials --resource-group JCR-staging --name jcr-argotest20-peter
# does a merge.

# --node-vm-size Standard_D2s_v3 
# actually: Standard_DS2_v2  16GB
# B4ms? A2m_v2? E2as_v4
kubectl config get-contexts
echo $GIT_TOKEN  $GIT_REPO

kubectl cluster-info
#nginx ingress is required for argo DNS.
# fix dns at dns provider
# fix names in argo repo

kubectl patch storageclass default -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}' 
#switch over to argo, gitlab.com/jointcyberrange.nl/jcr-deploy-by-argocd.git
```

# Cluster setup
For an extensive list of options see https://learn.microsoft.com/en-us/cli/azure/aks?view=azure-cli-latest.
```bash az
#  Create a new AKS cluster and attach a public IP for your nodes. Takes a few minutes.

time az aks create -g JCR-staging -n jcr-argotest12 -l westeurope  --enable-node-public-ip --generate-ssh-keys --nodepool-name k8nodes  --enable-addons http_application_routing

az aks nodepool update \
  --resource-group <resourceGroup> \
  --cluster-name <clusterName> \
  --name <nodepoolName> \
  --nodepool-allowed-host-ports 80/tcp,443/tcp,53/udp,40000-60000/tcp,40000-50000/udp\
  --nodepool-asg-ids "<asgId>,<asgId>"
# allowed host ports no longer works. 

az aks create \
  --resource-group <resourceGroup> \
  --name <clusterName> \
  --nodepool-name <nodepoolName> \
  --nodepool-allowed-host-ports 80/tcp,443/tcp,53/udp,40000-60000/tcp,40000-50000/udp\
  --nodepool-asg-ids "<asgId>,<asgId>"

  az aks create \
  --resource-group JCR-staging \
  --name jcr-argotest12\
  --nodepool-name argo12 \
  --nodepool-allowed-host-ports 80/tcp,443/tcp,53/udp,30000-60000/tcp,30000-50000/udp \
  -l westeurope
# there are more options to this. But it seems to not work. 
#  --nodepool-asg-ids "<asgId>,<asgId>"
# --kubernetes-version 
# --enable-blob-driver ? 
# --tier free
# --zones -z
# --ssh-key-value


az aks get-credentials --resource-group myResourceGroup --name myAKSCluster [--admin] -f -
az aks get-credentials --resource-group JCR-staging --name myAKSCluster [--admin] -f -
az aks get-credentials --resource-group JCR-staging --name jcr-argotest10 --admin
```
Current setup
```bash

#forget below
time az aks create -g JCR-staging -n jcr-argotest12-peter -l westeurope  --enable-node-public-ip --generate-ssh-keys --nodepool-name k8nodes  --enable-addons http_application_routing
time az aks nodepool update -g JCR-staging --cluster-name jcr-argotest14 --nodepool-name k8nodes --nodepool-allowed-host-ports 80/tcp,443/tcp,53/udp,30000-60000/tcp,30000-50000/udp 
# unrecognized arguments: --nodepool-allowed-host-ports; did it work before? 
# time az aks create -g JCR-staging -n jcr-argotest11 -l westeurope  --enable-node-public-ip --generate-ssh-keys --nodepool-allowed-host-ports 80/tcp,443/tcp,53/udp,30000-60000/tcp,30000-50000/udp 


az aks nodepool update \
  --resource-group JCR-staging \
  --cluster-name jcr-argotest12 \
  --name k8nodes \
  --nodepool-allowed-host-ports 80/tcp,443/tcp,53/udp,40000-60000/tcp,40000-50000/udp\
  --nodepool-asg-ids
  
   "<asgId>,<asgId>"


```
Then it's time for kubectl magic

# Application setup
```bash
kubectl patch storageclass default -p '{"metadata": {"annotations":{"storageclass.kubernetes.io/is-default-class":"false"}}}' 
kubectl apply -f echoserver.yml

# move over to argo bootstrap
```

# Diagnostics
```bash az

# list resources and clusters:
#
az aks list -o table
# list resource groups
az group list -o table
# list nodepools
az aks nodepool list --cluster-name jcr-jesse-argotest5 --resource-group jcr-jesse-argotest5_group
az aks nodepool list --cluster-name jcr-argotest20-peter --resource-group JCR-staging
# list IP addresses
az vmss list-instance-public-ips --resource-group MC_jcr-jesse-argotest5_group_jcr-jesse-argotest5_westeurope --name aks-nodepool3-22968764-vmss
az vmss list-instance-public-ips --resource-group MC_JCR-staging_jcr-argotest8_westeurope --name aks-nodepool1-32652124-vmss
az vmss list-instance-public-ips --resource-group mc_jcr-staging_jcr-argotest15-peter_westeurope --name aks-argo15peter-41928322-vmss

kubectl cluster-info
# control plane is required for DNS?
#nginx ingress is required for argo DNS.

kubectl describe nodes

kubectl get services -A |grep ngin

az account show --query tenantId --output tsv

az network nsg list --out table

az network nsg show --resource-group mc_jcr-staging_jcr-argotest11_westeurope --name aks-agentpool-33836205-nsg
# kunnen we de namen hiervan niet zelf kiezen? 
az network nsg create --resource-group JCR-staging --name myNSG
# blijkbaar wel dus. Maar hoe nu aan de nodepool koppelen.
# en hoe rules er in zetten..

```

# Cleanup
```bash
az aks delete --name MyManagedCluster --resource-group MyResourceGroup
az aks delete --resource-group JCR-staging --name jcr-argotest12-peter 
```

